import calendar
date = input("Enter a date: ")
day_in_week = calendar.weekday(int(date[6:]) , int(date[3:5]) , int(date[0:2]))
if day_in_week == 0:
	print("Monday")
elif day_in_week == 1:
	print("Tuesday")
elif day_in_week == 2:
	print("Wednesday")
elif day_in_week == 3:
	print("Thursday")
elif day_in_week == 4:
	print("Friday")
elif day_in_week == 5:
	print("Saturday")
elif day_in_week == 6:
	print("Sunday")
