guess_letter = input("Guess a letter: ")
if (len(guess_letter) > 1) and (guess_letter.isalpha()):
	print("E1")
elif not(guess_letter.isalpha()) and ((len(guess_letter) == 1)):
	print("E2")
elif (len(guess_letter) > 1) and (not(guess_letter.isalpha())):
	print("E3")
else: 
	print(guess_letter.lower())
