BOOM_NUM = 7
BOOM_IN_NUM = '7'

def seven_boom(end_number):
	"""
	This function same the game seven boom.
	:param end_number: the number that play from zero until arrive to him.
	:type end_number: int.
	:return: List with all the numbers in the game and 'BOOM' if is need to be boom.
	:rtype: list.
	"""
	list = []
	for num in range (end_number + 1):
		if num % BOOM_NUM == 0 or BOOM_IN_NUM in str(num):
			list += ['BOOM']
		else:
			list += [num]
	return list
