def squared_numbers(start, stop):
	"""
	This function build list with square of all the numbers between the parameters.
	:param start: The first num.
	:type start: int.
	:param stop: The last num.
	:type stop: int.
	:return: List with all of the results.
	:rtype: list.
	"""
	list = []
	while start <= stop:
		list += [start ** 2]
		start += 1
	return list
