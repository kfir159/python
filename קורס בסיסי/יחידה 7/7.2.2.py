def numbers_letters_count(my_str):
	"""
	This function get string and return list with count digits and count the else chars.
	:param my_str: The string.
	:type my_str: str.
	:return: The list with two vars - the count of digits and count the else chars.
	:rtype: list.
	"""
	count_digits, count_else = 0, 0
	for char in my_str:
		if char.isdigit():
			count_digits += 1
		else:
			count_else += 1
	return [count_digits, count_else]
