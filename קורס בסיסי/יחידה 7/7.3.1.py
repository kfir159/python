def show_hidden_word(secret_word, old_letters_guessed):
	"""
	This function show to the player the letters that he guessed true and where and what he doesn't guessed and where.
	:param secret_word: The secret word that the player need guess.
	:type secret_word: str.
	:param old_letters_guessed: All the letters that the player guessed.
	:type old_letters_guessed: list.
	:return: The String that showed to the player.
	:rtype: str.
	"""
	hidden_word = ["_"] * len(secret_word)
	counter = -1
	for letter in secret_word:
		counter += 1
		if letter in old_letters_guessed:
			hidden_word[counter] = letter
	return ' '.join(hidden_word)
