def is_greater(my_list, n):
	"""
	This function get a list and number and return list with all the numbers that bigger then the number.
	:param my_list: The list of numbers.
	:type my_list: list.
	:param n: The number that need bigger then him.
	:type n: int.
	:return: The list with the numbers that bigger then n.
	:rtype: list.
	"""
	result = []
	for num in my_list:
		if num > n:
			result += [num]
	return result
