def check_win(secret_word, old_letters_guessed):
	"""
	This function check if the player won.
	:param secret_word: The secret word that the player need guess.
	:type secret_word: str.
	:param old_letters_guessed: All the letters that the player guessed.
	:type old_letters_guessed: list.
	:return: True or false if the player won.
	:rtype: bool.
	"""
	for letter in secret_word:
		if letter not in old_letters_guessed:
			return False
	return True
