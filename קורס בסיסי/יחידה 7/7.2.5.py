def sequence_del(my_str):
	"""
	This function create string from string with sequences.
	:param my_str: The string with the sequences.
	:type my_str: str.
	:return: String without the sequences.
	:rtype: str.
	"""
	new_str = my_str[0]
	for i in range(1, len(my_str)):
		if my_str[i] != my_str[i - 1]:
			new_str += my_str[i]
	return new_str
