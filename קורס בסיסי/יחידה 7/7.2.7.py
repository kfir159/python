def arrow(my_char, max_length):
	"""
	This function make arrow from char in some length.
	:param my_char: The char to the arrow.
	:type my_char: str.
	:param max_length: The length of the arrow.
	:type max_length: int.
	:return: The string with the arrow.
	:rtype: str.
	"""
	str = ""
	if max_length == 1:
		return my_char
	for char1 in range(1,max_length + 1):
		str = str + (my_char * char1) + "\n"
	for char2 in reversed (range(1, max_length)):
		if char2 != 1:
			str  = str + (my_char * char2) + "\n"
		else:
			str  = str + (my_char * char2)
	return str
