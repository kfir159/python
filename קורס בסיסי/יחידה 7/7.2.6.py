EXIT = 9

def option_1(list_products):
	"""
	This function print the products.
	:param list_products: The list of products.
	:type list_products: list.
	:return: None.
	"""
	print(list_products)

def option_2(list_products):
	"""
	This function print the len of list products.
	:param list_products: The list of products.
	:type list_products: list.
	:return: None.
	"""
	print(len(list_products))

def option_3(list_products):
	"""
	This function check if specific product exist in list products.
	:param list_products: The list of products.
	:type list_products: list.
	:return: None.
	"""
	check_product = input("enter product: ")
	print(check_product in list_products)

def option_4(list_products):
	"""
	This function print count how much times specific product exist
	:param list_products: The list of products.
	:type list_products: list.
	:return: None.
	"""
	check_product = input("enter product: ")
	print(list_products.count(check_product))

def option_5(list_products):
	"""
	This function remove one product from the list.
	:param list_products: The list of products.
	:type list_products: list.
	:return: The list after the remove.
	:rtype: list.
	"""
	remove_product = input("enter product to delete: ")
	if remove_product in list_products:
		list_products.remove(remove_product)
	return list_products

def option_6(list_products):
	"""
	This function append one product to the list.
	:param list_products: The list of products.
	:type list_products: list.
	:return: The list after the append.
	:rtype: list.
	"""
	append_product = input("enter product to append: ")
	list_products.append(append_product)
	return list_products

def option_7(list_products):
	"""
	This function print the all product that illegal
	:param list_products: The list of products.
	:type list_products: list.
	:return: None.
	"""
	for product in list_products:
		if len(product) < 3 or not product.isalpha():
			print(product)

def option_8(list_products):
	"""
	This function remove the duplicate products from the list.
	:param list_products: The list of products.
	:type list_products: list.
	:return: The list after the remove.
	:rtype: list.
	"""
	return list(set(list_products))

def main():
	str_product = input("enter list of product in string: ")
	list_products = str_product.split(",")
	num_in_menu = 0
	while num_in_menu != EXIT:
		num_in_menu = int(input("enter num: "))
		if num_in_menu == 1:
			option_1(list_products)
		elif num_in_menu == 2:
			option_2(list_products)
		if num_in_menu == 3:
			option_3(list_products)
		if num_in_menu == 4:
			option_4(list_products)
		if num_in_menu == 5:
			list_products = option_5(list_products)
		if num_in_menu == 6:
			list_products = option_6(list_products)
		if num_in_menu == 7:
			option_7(list_products)
		if num_in_menu == 8:
			list_products = option_8(list_products)

if __name__ == "__main__":
	main()
