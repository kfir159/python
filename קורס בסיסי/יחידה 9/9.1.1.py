def are_files_equal(file1, file2):
	"""
	This function check if the files same in their text.
	:param file1: The first path of file.
	:type file1: str.
	:param file2: The second path of file.
	:type file2: str.
	:return: True or false if the files are same.
	:rtype: bool.
	"""
	file1_open = open(file1 , 'r').read()
	file2_open = open(file2 , 'r').read()
	return file1_open == file2_open
