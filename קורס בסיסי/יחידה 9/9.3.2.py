def my_mp4_playlist(file_path, new_song):
    """
    This function replace the name of the third song to new song.
    :param file_path: The path of the file.
    :type file_path: str.
    :param new_song: The new song to the third song.
    :type new_song: str.
    :return: The text of the new file after the update.
    :rtype: str.
    """
    file = open(file_path, "r")
    file_text = file.read()
    while(file_text.count('\n') < 2):
        file_text += "\n"
    list_songs = file_text.split("\n")
    if ';' not in list_songs[2]:
        list_songs[2] += ';'
    list_of_songs_list = []
    for song in list_songs:
        list_of_songs_list += [song.split(";")]
    list_of_songs_list[2][0] = new_song
    new_list = []
    for list in list_of_songs_list:
        new_list += [";".join(list)]
    file.close()
    file = open(file_path, "w")
    file.write("\n".join(new_list))
    file.close()
    return "\n".join(new_list)
