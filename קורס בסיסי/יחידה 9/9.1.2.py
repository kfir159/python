def sort(file_text):
	"""
	This function get text from file and and print all the words in him sorted and without duplicates.
	:param file_text: The text from the file.
	:type file_text: str.
	:return: None.
	"""
	print(sorted(set(file_text.split())))

def rev(file):
	"""
	This function get text from file and print his lines reverse.
	:param file: The file.
	:type file: file.
	:return: None.
	"""
	for line in file.readlines():
		print(line[-1::-1])

def last(file):
	"""
	This function input number and print this end lines according the number.
	:param file_text: The text from the file.
	:type file_text: file.
	:return: None.
	"""
	num = int(input("Enter a number: "))
	list_lines = file.readlines()
	i = len(list_lines) - num
	while(len(list_lines) > i):
		print(list_lines[i][:-1])
		i += 1

def main():
	file_path = input("Enter a file path: ")
	action = input("Enter the action: ")
	if action == "sort":
		file_text = open(file_path, 'r').read()
		sort(file_text)
	if action == "rev":
		file = open(file_path, 'r')
		rev(file)
	if action == "last":
		file = open(file_path, 'r')
		last(file)

if __name__ == "__main__":
	main()
