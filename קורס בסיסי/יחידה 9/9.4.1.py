def choose_word(file_path, index):
	"""
	This function return the sum of different words in the file and the chosen word.
	:param file_path: The path of the file of words.
	:type file_path: str.
	:param index: The index of the word that the player choose.
	:return: tuple with num of different words and the chosen word.
	:rtype: tuple.
	"""
	file_list = open(file_path, 'r').read().split()
	sum_words = len(set(file_list))
	real_index = (index % len(file_list)) - 1
	the_word = file_list[real_index]
	return sum_words, the_word
