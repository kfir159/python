def who_is_missing(file_path):
	"""
	This function find the missing number in the file.
	:param file_path: The path of the file.
	:type file_path: str.
	:return: The number that missing.
	:rtype: int.
	"""
	file = open(file_path, 'r')
	file_list = file.read().split(",")
	the_mistory_num = 0
	file.close()
	for i in range(1, len(file_list) + 2):
		if i not in file_list:
			the_mistory_num = i
			break
	new_file = open("found.txt", 'w')
	new_file.write(str(the_mistory_num))
	return the_mistory_num
