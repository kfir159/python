def my_mp3_playlist(file_path):
    """
    This function find in file the song that longest, how mach songs there are and who is the player that exist the most.
    :param file_path: The path of the file.
    :type file_path: str.
    :return: The song that longest, how mach songs there are and who is the player that exist the most.
    :rtype: tuple.
    """
    file_text = open(file_path, "r").read()
    list_songs = file_text.split("\n")
    list_of_songs_list = []
    for song in list_songs:
        list_of_songs_list += [song.split(";")[:-1]]
    list_len = []
    for list in list_of_songs_list:
        list_len += [float(list[2].split(":")[0]) + float(list[2].split(":")[1]) / 60]
    max_len = max(list_len)
    index = list_len.index(max_len)
    longer_song = list_of_songs_list[index][0]
    count_songs = len(list_of_songs_list)
    dict_players = {}
    for list in list_of_songs_list:
        if list[1] in dict_players.keys():
            dict_players[list[1]] += 1
        else:
            dict_players[list[1]] = 1
    max_player = max(dict_players,key=dict_players.get)
    return longer_song, count_songs, max_player
