def copy_file_content(source, destination):
	"""
	This function copy the text from the source file to the destination file.
	:param source: The source file.
	:type source: str.
	:param destination: The destination file.
	:type destination: str.
	:return: None.
	"""
	copy_file = open(source, "r")
	dest_file = open(destination, 'w')
	dest_file.write(copy_file.read())
	copy_file.close()
	dest_file.close()
