import os

MAX_TRIES = 6

def print_start():
	"""
	This function print the start screen of the game.
	:return: None.
	"""
	print("""  _    _                                         
 | |  | |                                        
 | |__| | __ _ _ __   __ _ _ __ ___   __ _ _ __  
 |  __  |/ _` | '_ \ / _` | '_ ` _ \ / _` | '_ \ 
 | |  | | (_| | | | | (_| | | | | | | (_| | | | |
 |_|  |_|\__,_|_| |_|\__, |_| |_| |_|\__,_|_| |_|
					  __/ |                      
					 |___/ """)

def print_hangman(num_of_tries):
	"""
	This function get num of tries and print the state of the game.
	:param num_of_tries: The num of tries until now.
	:type num_of_tries:
	:return: None.
	"""
	HANGMAN_PHOTOS = {
		1: """x-------x""",
		2: """
			x-------x
			|
			|
			|
			|
			|
			""",
		3: """
			x-------x
			|       |
			|       0
			|
			|
			|
		""",
		4:
		"""
			x-------x
			|       |
			|       0
			|       |
			|
			|
		""",
		5:
		"""
			x-------x
			|       |
			|       0
			|      /|\\
			|
			|
		""",
		6:
		"""
			x-------x
			|       |
			|       0
			|      /|\\
			|      /
			|
		""",
		7:
		"""
			x-------x
			|       |
			|       0
			|      /|\\
			|      / \\
			|
		"""
		}
	print(HANGMAN_PHOTOS[num_of_tries])

def check_win(secret_word, old_letters_guessed):
	"""
	This function check if the player won.
	:param secret_word: The secret word that the player need guess.
	:type secret_word: str.
	:param old_letters_guessed: All the letters that the player guessed.
	:type old_letters_guessed: list.
	:return: True or false if the player won.
	:rtype: bool.
	"""
	for letter in secret_word:
		if letter not in old_letters_guessed:
			return False
	return True

def show_hidden_word(secret_word, old_letters_guessed):
	"""
	This function show to the player the letters that he guessed true and where and what he doesn't guessed and where.
	:param secret_word: The secret word that the player need guess.
	:type secret_word: str.
	:param old_letters_guessed: All the letters that the player guessed.
	:type old_letters_guessed: list.
	:return: The String that showed to the player.
	:rtype: str.
	"""
	hidden_word = ["_"] * len(secret_word)
	counter = -1
	for letter in secret_word:
		counter += 1
		if letter in old_letters_guessed:
			hidden_word[counter] = letter
	return ' '.join(hidden_word)

def check_valid_input(letter_guessed , old_letters_guessed):
	"""
	This function check if the guess is valid.
	:param letter_guessed: The letter that guessed.
	:type letter_guessed: str.
	:param old_letters_guessed: The old valid guessed.
	:type old_letters_guessed: list.
	:return: True or false if the guess is valid.
	:rtype: bool.
	"""
	return len(letter_guessed) == 1 and letter_guessed.isalpha() and letter_guessed.lower() not in old_letters_guessed

def try_update_letter_guessed(letter_guessed , old_letters_guessed):
	"""
	This function get guess and the list of old guesses and if the guess is valid, append him to the list
	and if the guess is invalid print X and the old guesses.
	:param letter_guessed: The letter that guessed.
	:type letter_guessed: str.
	:param old_letters_guessed: The list of the guessed letters.
	:type old_letters_guessed: list.
	:return: True or false if the guess is valid.
	:rtype: bool.
	"""
	if check_valid_input(letter_guessed, old_letters_guessed):
		old_letters_guessed.append(letter_guessed.lower())
		return True
	else:
		print("X")
		if(len(old_letters_guessed) > 0):
			old_letters_guessed.sort()
			str = (" -> ".join(old_letters_guessed))
			print(str)
		return False

def choose_word(file_path, index):
	"""
	This function return the sum of different words in the file and the chosen word.
	:param file_path: The path of the file of words.
	:type file_path: str.
	:param index: The index of the word that the player choose.
	:return: tuple with num of different words and the chosen word.
	:rtype: tuple.
	"""
	file_list = open(file_path, 'r').read().split()
	real_index = (index % len(file_list)) - 1
	the_word = file_list[real_index]
	return the_word

def main():
	print_start()
	file_path = input("Enter file path: ")
	if not os.path.exists(file_path):
		print("The file doesn't exist.")
		exit(1)
	index = int(input("Enter index: "))
	print("Let's start!")
	secret_word = choose_word(file_path, index)
	old_letters_guessed = []
	num_of_tries = 1
	print_hangman(num_of_tries)
	won = False
	while num_of_tries <= MAX_TRIES and not won:
		print(show_hidden_word(secret_word, old_letters_guessed))
		the_letter_is_valid = False
		letter_guessed = ""
		while not the_letter_is_valid:
			letter_guessed = input("Guess a letter: ")
			the_letter_is_valid = try_update_letter_guessed(letter_guessed, old_letters_guessed)
		#old_letters_guessed += letter_guessed
		if letter_guessed.lower() not in secret_word:
			num_of_tries += 1
			print_hangman(num_of_tries)
		won = check_win(secret_word, old_letters_guessed)
	print(show_hidden_word(secret_word, old_letters_guessed))
	if won:
		print("WIN")
	else:
		print("LOSE")

if __name__ == "__main__":
	main()
