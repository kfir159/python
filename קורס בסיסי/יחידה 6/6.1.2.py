def shift_left(my_list):
	"""
	This function do shift left to list.
	:param my_list: The list to shift.
	:type my_list: list.
	:return: New list after the shift.
	:rtype: list.
	"""
	return [my_list[1], my_list[2], my_list[0]]
