def check_valid_input(letter_guessed , old_letters_guessed):
	"""
	This function check if the guess is valid.
	:param letter_guessed: The letter that guessed.
	:type letter_guessed: str.
	:param old_letters_guessed: The old valid guessed.
	:type old_letters_guessed: list.
	:return: True or false if the guess is valid.
	:rtype: bool.
	"""
	return len(letter_guessed) == 1 and letter_guessed.isalpha() and letter_guessed.lower() not in old_letters_guessed

def try_update_letter_guessed(letter_guessed , old_letters_guessed):
	"""
	This function get guess and the list of old guesses and if the guess is valid, append him to the list
	and if the guess is invalid print X and the old guesses.
	:param letter_guessed: The letter that guessed.
	:type letter_guessed: str.
	:param old_letters_guessed: The list of the guessed letters.
	:type old_letters_guessed: list.
	:return: True or false if the guess is valid.
	:rtype: bool.
	"""
	if check_valid_input(letter_guessed, old_letters_guessed):
		old_letters_guessed.append(letter_guessed.lower())
		return True
	else:
		print("X")
		old_letters_guessed.sort()
		str = (" -> ".join(old_letters_guessed))
		print(str)
		return False
