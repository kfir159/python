def extend_list_x(list_x, list_y):
    """
    This function get two lists and connect them to one.
    :param list_x: The first list.
    :type list_x: list.
    :param list_y: The second list.
    :type list_y: list.
    :return: The connected list.
    :rtype: list.
    """
    list_x[:0] = list_y
    return list_x
