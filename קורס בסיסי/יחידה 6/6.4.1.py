def check_valid_input(letter_guessed , old_letters_guessed):
	"""
	This function check if the guess is valid.
	:param letter_guessed: The letter that guessed.
	:type letter_guessed: str.
	:param old_letters_guessed: The old valid guessed.
	:type old_letters_guessed: list.
	:return: True or false if the guess is valid.
	:rtype: bool.
	"""
	return len(letter_guessed) == 1 and letter_guessed.isalpha() and letter_guessed.lower() not in old_letters_guessed
