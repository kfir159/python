def format_list(my_list):
    """
    This function connect the even vars and the last var in list.
    :param my_list: The list.
    :type my_list: list.
    :return: The string of the connect vars.
    :rtype: str.
    """
    return ", ".join(my_list[::2]) + ", and " + my_list[-1]
