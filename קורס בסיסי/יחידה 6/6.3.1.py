def are_lists_equal(list1, list2):
	"""
	This function check if two lists have the same values.
	:param list1: The first list.
	:type list1: list.
	:param list2: The second list.
	:type list2: list.
	:return: True or false if they have the same values.
	:rtype: bool.
	"""
	return list1.sort() == list2.sort()
