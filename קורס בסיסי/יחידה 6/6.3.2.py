def longest(my_list):
	"""
	This function return the bigger word from list.
	:param my_list: List with words.
	:type my_list: list.
	:return: The word that longer.
	:rtype: srt.
	"""
	my_list.sort(key = len)
	return my_list[len(my_list) - 1]
