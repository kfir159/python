def is_valid_input(letter_guessed):
    """
    This function check the validation of letter that guessed.
    :param letter_guessed: The letter to check.
    :type letter_guessed: str.
    :return: True or False if the letter is valid.
    :rtype: bool.
    """
    return (len(letter_guessed) == 1) and (letter_guessed.isalpha())
