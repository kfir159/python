MIN_AGE = 13
MAX_AGE = 19
SPECIAL_AGE_1 = 15
SPECIAL_AGE_2 = 16

def fix_age(age):
    if MIN_AGE <= age <= MAX_AGE and age != SPECIAL_AGE_1 and age != SPECIAL_AGE_2:
        return 0
    else:
        return age

def filter_teens(a=MIN_AGE, b=MIN_AGE, c=MIN_AGE):
    a = fix_age(a)
    b = fix_age(b)
    c = fix_age(c)
    return a + b + c
