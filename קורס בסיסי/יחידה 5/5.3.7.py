SMALL_CUBE = 1
BIG_CUBE = 5

def chocolate_maker(small, big, x):
	return (x % BIG_CUBE <= small) and (big * BIG_CUBE + SMALL_CUBE * small >= x)
