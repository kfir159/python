def func(num1, num2):
	"""
	This func sum all of the results of the basic operations between two numbers.
	:param num1: Number.
	:param num2: Number.
	:type num1: int.
	:type num2: int.
	:return: the Result.
	:rtype: Float.
	"""
	help(func)
	return (num1 + num2) + (num1 - num2) + (num1 * num2) + (num1 / num2)

def main():
	func(3, 5)
	
if __name__ == "__main__":
	main()
