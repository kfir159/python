def inverse_dict(my_dict):
	"""
	This function create dict with reverse between the keys and the values.
	:param my_dict: The dict to reverse.
	:type my_dict: dict.
	:return: The reversed dict.
	:rtype: dict.
	"""
	new_dict = {}
	for key in my_dict:
		if my_dict[key] not in new_dict:
			new_dict[my_dict[key]] = [key]
		else:
			new_dict[my_dict[key]] += [key]
		new_dict[my_dict[key]].sort()
	return new_dict
