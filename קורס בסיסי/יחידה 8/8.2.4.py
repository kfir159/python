def sort_anagrams(list_of_strings):
    """
    This function get strings without spaces and return all anagrams as lists in one big list.
    :param list_of_strings: list of words.
    :type list_of_strings: list.
    :return: list of all anagrams as sorted lists for every anagram group.
    :rtype: list.
    """
    big_list = []
    for item in list_of_strings:
        sorted_list = []
        for j in list_of_strings:
            if sorted(j) == sorted(item):
                sorted_list += [j]
        if sorted_list not in big_list:
            big_list.append(sorted_list)
    return big_list
