def get_key(product_tuple):
	"""
	This function give key to sort the list.
	:param product_tuple: tuple with product and price.
	:type product_tuple: tuple.
	:return: the second value in the tuple, the price.
	:rtype: int.
	"""
	return product_tuple[1]

def sort_prices(list_of_tuples):
	"""
	This function sort the list of tuples from the highest price to the lowest price.
	:param list_of_tuples: list of tuples of products and prices.
	:type list_of_tuples: list.
	:return: list of tuples after sort.
	:rtype: list.
	"""
	return sorted(list_of_tuples , key = get_key , reverse = True)
