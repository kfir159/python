def print_hangman(num_of_tries):
    """
    This function get num of tries and print the state of the game.
    :param num_of_tries: The num of tries until now.
    :type num_of_tries:
    :return: None.
    """
    HANGMAN_PHOTOS = {
        1: """x-------x""",
        2: """
            x-------x
            |
            |
            |
            |
            |
            """,
        3: """
            x-------x
            |       |
            |       0
            |
            |
            |
        """,
        4:
        """
            x-------x
            |       |
            |       0
            |       |
            |
            |
        """,
        5:
        """
            x-------x
            |       |
            |       0
            |      /|\\
            |
            |
        """,
        6:
        """
            x-------x
            |       |
            |       0
            |      /|\\
            |      /
            |
        """,
        7:
        """
            x-------x
            |       |
            |       0
            |      /|\\
            |      / \\
            |
        """
        }
    print(HANGMAN_PHOTOS[num_of_tries])
