def count_chars(my_str):
	"""
	This function create dict with all the letters in the string and the count of the shows of each one.
	:param my_str: The string.
	:type my_str: str.
	:return: The dict with the letters and count there shows.
	:rtype: dict.
	"""
	my_dict = {}
	my_str = my_str.replace(" " , "")
	for letter in my_str:
		if letter not in my_dict:
			my_dict[letter] = 1
		else:
			my_dict[letter] += 1
	return my_dict
