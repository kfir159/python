def mult_tuple(tuple1, tuple2):
	"""
	This function do multiply of two tuples.
	:param tuple1: The first tuple to multiply.
	:type tuple1: tuple.
	:param tuple2: The second tuple to multiply.
	:return: Tuple result after the multiply.
	:rtype: tuple.
	"""
	multy_tuple = ()
	for i in tuple1:
		for j in tuple2:
			multy_tuple += ((i, j),(j, i),)
	return multy_tuple
