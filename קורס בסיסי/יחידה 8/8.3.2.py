import datetime

def option_1(mariah_dict):
	"""
	This function print the last name of Mariah.
	:param mariah_dict: The dict with the details of Mariah.
	:type mariah_dict: dict.
	:return: None.
	"""
	print(mariah_dict["last_name"])
	
def option_2(mariah_dict):
	"""
	This function print the month that Mariah was born.
	:param mariah_dict: The dict with the details of Mariah.
	:type mariah_dict: dict.
	:return: None.
	"""
	print(mariah_dict["birth_date"][3:5])
	
def option_3(mariah_dict):
	"""
	This function print the count of hobbies of Mariah
	:param mariah_dict: The dict with the details of Mariah.
	:type mariah_dict: dict.
	:return: None.
	"""
	print(len(mariah_dict["hobbies"]))
	
def option_4(mariah_dict):
	"""
	This function print the last hobby from the list hobbies of Mariah
	:param mariah_dict: The dict with the details of Mariah.
	:type mariah_dict: dict.
	:return: None.
	"""
	print(mariah_dict["hobbies"][-1])
	
def option_5(mariah_dict):
	"""
	This function append to the list hobbies of Mariah the hobby cooking.
	:param mariah_dict: The dict with the details of Mariah.
	:type mariah_dict: dict.
	:return: None.
	"""
	mariah_dict["hobbies"] += ["Cooking"]
	
def option_6(mariah_dict):
	"""
	This function print the birth date of Mariah in tuple.
	:param mariah_dict: The dict with the details of Mariah.
	:type mariah_dict: dict.
	:return: None.
	"""
	birth_date = (mariah_dict["birth_date"][0:2],mariah_dict["birth_date"][4:5],mariah_dict["birth_date"][6:])
	print(mariah_dict["birth_date"])
	
def option_7(mariah_dict):
	"""
	This function print the age of Mariah.
	:param mariah_dict: The dict with the details of Mariah.
	:type mariah_dict: dict.
	:return: None.
	"""
	birthdate_obj = datetime.datetime.strptime(mariah_dict["birth_date"], "%d.%m.%Y")
	current_datetime = datetime.datetime.now()
	age_timedelta = current_datetime - birthdate_obj
	age_years = int(age_timedelta.total_seconds() / (365.25 * 24 * 60 * 60))
	mariah_dict["age"] = age_years
	print(mariah_dict["age"])
	
def main():
	mariah_dict = {"first_name" : "Mariah" , "last_name" : "Carey" , "birth_date" : "27.03.1970" , "hobbies" : ["Sing" , "Compose" , "Act"]}
	choose_option = int(input("enter your choice: "))
	dict_options = {1 : option_1, 2 : option_2, 3 : option_3, 4 : option_4, 5 : option_5, 6 : option_6, 7 : option_7}
	if 1 <= choose_option <= 7:
		dict_options[choose_option](mariah_dict)

if __name__ == "__main__":
	main()
