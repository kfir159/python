class BigThing:
    """
    This class is a thing.
    """
    def __init__(self, thing):
        """
        This function init the thing.
        :param thing: something that the class get.
        :type thing: anything.
        """
        self._thing = thing

    def size(self):
        """
        This function check if the thing is number and return him. if not, check if list/dict or string and return the len.
        :return: Or the number or the len of list/dict/string.
        :rtype: int.
        """
        if isinstance(self._thing, int): return self._thing
        elif isinstance(self._thing, list) or isinstance(self._thing, dict) or isinstance(self._thing, str): return len(self._thing)

class BigCat(BigThing):
    """
    This class is a big cat that extends from the class BigThing.
    """
    def __init__(self, thing, weight):
        """
        This function init the big cat with weight and thing.
        :param thing: The something.
        :type thing: anything.
        :param weight: The weight of the cat.
        :type weight: int.
        """
        super().__init__(thing)
        self._weight = weight

    def size(self):
        """
        This function check the weight of the cat and return a message.
        :return: Message according the weight of the cat.
        :rtype: str.
        """
        if self._weight > 20: return "Very Fat"
        elif self._weight > 15: return "Fat"
        else: return "OK"

def main():
    cutie = BigCat("mitzy", 22)
    print(cutie.size())

if __name__ == '__main__':
    main()
