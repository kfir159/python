class Octopus:
    """
    This class represents an octopus.
    """
    count_animals = 0
    def __init__(self, name="Octavio"):
        """
        This function init the octopus and add the count of octopuses.
        """
        self._name = name
        self._age = 0
        Octopus.count_animals += 1

    def birthday(self):
        """
        This function add one year to the age of the octopus.
        :return: None.
        """
        self._age += 1

    def get_age(self):
        """
        This function return the age of the octopus.
        :return: The age of the octopus.
        :rtype: int.
        """
        return self._age

    def set_name(self, name):
        """
        This function set a new name to the octopus.
        :param name: The new name.
        :type name: str.
        :return: None.
        """
        self._name = name

    def get_name(self):
        """
        This function return the name of the octopus.
        :return: The age of the octopus.
        :rtype: str.
        """
        return self._name

def main():
    oc1 = Octopus("Kfir")
    oc2 = Octopus()
    print(oc1.get_name())
    print(oc2.get_name())
    oc2.set_name("Eyal")
    print(oc2.get_name())
    print(Octopus.count_animals)

if __name__ == '__main__':
    main()
