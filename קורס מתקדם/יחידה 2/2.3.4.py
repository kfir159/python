class Pixel:
    """
    This class represents a pixel.
    """
    def __init__(self, x=0, y=0, red=0, green=0, blue=0):
        """
        This function init the properties of the pixel.
        :param x: The X cord.
        :param y: The y cord.
        :param red: The power of the red color.
        :param green: The power of the green color.
        :param blue: The power of the blue color.
        """
        self._x, self._y = x, y
        self._red, self._green, self._blue = red, green, blue

    def set_coords(self, x, y):
        """
        This function set a new cords to the pixel.
        :param x: The X cord.
        :param y: The Y cord.
        :return: None.
        """
        self._x, self._y = x, y

    def set_grayscale(self):
        """
        This function set the pixel to gray color.
        :return: None.
        """
        avg = (self._blue + self._red + self._green) / 3
        self._red, self._blue, self._green = avg, avg, avg

    def print_pixel_info(self):
        """
        This function print the properties of the pixel.
        :return: None.
        """
        to_print = ""
        if self._red == 0 and self._green == 0 and self._blue > 50:
            to_print = "Blue"
        elif self._red == 0 and self._green > 50 and self._blue == 0:
            to_print = "Green"
        elif self._red > 50 and self._green == 0 and self._blue == 0:
            to_print = "Red"
        print(f"X: {self._x}, Y: {self._y}, Color: ({self._red:.0f}, {self._green:.0f}, {self._blue:.0f})", to_print)

def main():
    my_pixel = Pixel(5, 6, 250)
    my_pixel.print_pixel_info()
    my_pixel.set_grayscale()
    my_pixel.print_pixel_info()

if __name__ == '__main__':
    main()
