class Animal:
    """
    This class represents animal.
    """

    zoo_name = "Hayaton"

    def __init__(self, name, hunger=0):
        """
        This function init the animal with name and hungry level.
        :param name: The name of the animal.
        :type name: str.
        :param hunger: The hunger level of the animal.
        :type hunger: int.
        """
        self._name = name
        self._hunger = hunger

    def get_name(self):
        """
        This function return the name of the animal.
        :return: The name of the animal.
        :rtype: str.
        """
        return self._name

    def is_hungry(self):
        """
        This function check if the animal is hungry.
        :return: True or false if the animal is hungry.
        :rtype: bool.
        """
        return self._hunger > 0

    def feed(self):
        """
        This function feed the animal and decrease his hungry level with one.
        :return: None.
        """
        self._hunger -= 1

    def talk(self):
        """
        This function exist in the specific animals.
        :return: None.
        """
        pass

class Dog(Animal):
    """
    This class represents the animal dog that extends from animal class.
    """

    def __init__(self, name, hunger=0):
        """
        This function init the animal dog with name and hungry level.
        :param name: The name of the dog.
        :type name: str.
        :param hunger: The hunger level of the dog.
        :type hunger: int.
        """
        super().__init__(name, hunger)

    def talk(self):
        """
        This function return what the dog saying.
        :return: What the dog saying.
        :rtype: str.
        """
        return "woof woof"

    def fetch_stick(self):
        """
        This function is a special action of the dog.
        :return: The string from the special action.
        :rtype: str.
        """
        print("There you go, sir!")

class Cat(Animal):
    """
    This class represents the animal cat that extends from animal class.
    """

    def __init__(self, name, hunger=0):
        """
        This function init the animal cat with name and hungry level.
        :param name: The name of the cat.
        :type name: str.
        :param hunger: The hunger level of the cat.
        :type hunger: int.
        """
        super().__init__(name, hunger)

    def talk(self):
        """
        This function return what the cat saying.
        :return: What the cat saying.
        :rtype: str.
        """
        return "meow"

    def chase_laser(self):
        """
        This function is a special action of the cat.
        :return: The string from the special action.
        :rtype: str.
        """
        print("Meeeeow")

class Skunk(Animal):
    """
    This class represents the animal cat that extends from animal class.
    """

    def __init__(self, name, hunger=0, stink_count=6):
        """
        This function init the animal skunk with name, hungry level and stink count.
        :param name: The name of the skunk.
        :type name: str.
        :param hunger: The hunger level of the skunk.
        :type hunger: int.
        :param stink_count: The stink count of the skunk.
        :type stink_count: int.
        """
        super().__init__(name, hunger)
        self._stink_count = stink_count

    def talk(self):
        """
        This function return what the skunk saying.
        :return: What the skunk saying.
        :rtype: str.
        """
        return "tsssss"

    def stink(self):
        """
        This function is a special action of the skunk.
        :return: The string from the special action.
        :rtype: str.
        """
        print("Dear lord!")

class Unicorn(Animal):
    """
    This class represents the animal unicorn that extends from animal class.
    """

    def __init__(self, name, hunger=0):
        """
        This function init the animal unicorn with name and hungry level.
        :param name: The name of the unicorn.
        :type name: str.
        :param hunger: The hunger level of the unicorn.
        :type hunger: int.
        """
        super().__init__(name, hunger)

    def talk(self):
        """
        This function return what the unicorn saying.
        :return: What the unicorn saying.
        :rtype: str.
        """
        return "Good day, darling"

    def sing(self):
        """
        This function is a special action of the unicorn.
        :return: The string from the special action.
        :rtype: str.
        """
        print("I’m not your toy...")

class Dragon(Animal):
    """
    This class represents the animal dragon that extends from animal class.
    """

    def __init__(self, name, hunger=0, color="Green"):
        """
        This function init the animal dragon with name, hungry level and color.
        :param name: The name of the dragon.
        :type name: str.
        :param hunger: The hunger level of the dragon.
        :type hunger: int.
        :param color: The color of the dragon.
        :type color: str.
        """
        super().__init__(name, hunger)
        self._color = color

    def talk(self):
        """
        This function return what the dragon saying.
        :return: What the dragon saying.
        :rtype: str.
        """
        return "Raaaawr"

    def breath_fire(self):
        """
        This function is a special action of the dragon.
        :return: The string from the special action.
        :rtype: str.
        """
        print("$@#$#@$")

def main():
    first_animal = Dog("Brownie", 10)
    second_animal = Cat("Zelda", 3)
    third_animal = Skunk("Stinky")
    forth_animal = Unicorn("Keith", 7)
    fifth_animal = Dragon("Lizzy", 1450)
    sixth_animal = Dog("Doggo", 80)
    seventh_animal = Cat("Kitty", 80)
    eighth_animal = Skunk("Stinky Jr.", 80)
    ninth_animal = Unicorn("Clair", 80)
    tenth_animal = Dragon("McFly", 80)
    zoo_lst = [first_animal, second_animal, third_animal, forth_animal, fifth_animal, sixth_animal, seventh_animal, eighth_animal, ninth_animal, tenth_animal]
    for animal in zoo_lst:
        if animal.is_hungry(): print(type(animal).__name__, animal.get_name())
        while animal.is_hungry():
            animal.feed()
        print(animal.talk())
        if(isinstance(animal, Dog)):
            animal.fetch_stick()
        elif(isinstance(animal, Cat)):
            animal.chase_laser()
        elif(isinstance(animal, Skunk)):
            animal.stink()
        elif(isinstance(animal, Unicorn)):
            animal.sing()
        elif(isinstance(animal, Dragon)):
            animal.breath_fire()
    print(Animal.zoo_name)

if __name__ == '__main__':
    main()
