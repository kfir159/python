class Octopus:
    """
    This class represented an octopus.
    """
    def __init__(self):
        """
        This function init the octopus.
        """
        self.name = "Octavio"
        self.age = 0

    def birthday(self):
        """
        This function add one year to the age of the octopus.
        :return: None.
        """
        self.age += 1

    def get_age(self):
        """
        This function return the age of the octopus.
        :return: The age of the octopus.
        :rtype: int.
        """
        return self.age

def main():
    oc1 = Octopus()
    oc2 = Octopus()
    oc1.birthday()
    print(oc1.get_age())
    print(oc2.get_age())

if __name__ == '__main__':
    main()
