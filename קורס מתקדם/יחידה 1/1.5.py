def long_name(names):
    """
    This function print the longest name from the list of names.
    :param names: The list of names.
    :type names: list.
    :return: None.
    """
    print(max(names, key=len))

def sum_length_of_names(names):
    """
    This function print the sum of all length of the names.
    :param names: The list of names.
    :type names: list.
    :return: None.
    """
    print(sum(len(name) for name in names))

def shortest_names(names):
    """
    This function print the shortest names.
    :param names: The list of names.
    :type names: list.
    :return: None.
    """
    print("\n".join([name for name in names if len(name) == len(min(names, key=len))]))

def all_length_of_names(names):
    """
    This function print all of the length of the names.
    :param names: The list of names.
    :type names: list.
    :return: None.
    """
    print("\n".join([str(len(name)) for name in names]))

def all_names_spesific_length(names, length):
    """
    This function print all the names that has specific len.
    :param names: The list of names.
    :type names: list.
    :param length: The specific length.
    :type length: int.
    :return: None.
    """
    print("\n".join([name for name in names if len(name) == length]))

def main():
    with open("names.txt", "r") as file:
        names = file.read().split("\n")
        long_name(names)
        sum_length_of_names(names)
        shortest_names(names)
        all_length_of_names(names)
        length = int(input("Enter len of names: "))
        all_names_spesific_length(names, length)

if __name__ == '__main__':
    main()
