def sum_letters(a):
    """
    This function get char and return him * 2.
    :param a: The char.
    :type a: str.
    :return: The char twice.
    :rtype: str.
    """
    return a * 2

def double_letter(my_str):
    """
    This function get a string and return new string that each char in him twice.
    :param my_str: The string to print.
    :type my_str: str.
    :return: New string that each char show twice.
    :rtype: str.
    """
    return ''.join(map(sum_letters, my_str))
