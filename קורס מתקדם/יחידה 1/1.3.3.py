def is_funny(string):
    """
    This function check if the string has only 'a' or 'h' chars.
    :param string: The string to check.
    :type string: str.
    :return: True of false if the string only 'a' or 'h'.
    :rtype: bool.
    """
    return all([char == 'h' or char == 'a' for char in string])
