import functools

def add(x, y):
    """
    This function get two numbers and add them.
    :param x: The first number.
    :type x: int.
    :param y: The second number.
    :type y: int.
    :return: The sum of the numbers.
    :rtype: int.
    """
    return x + y

def sum_of_digits(number):
    """
    This function get number and return the sum of the digits in the numbers.
    :param number: The number.
    :type number: int.
    :return: The sum digits of the number.
    :rtype: int.
    """
    return functools.reduce(add, list(map(int, str(number))))

print(sum_of_digits(104))
