def intersection(list_1, list_2):
    """
    This function return list with all of the numbers the shared with the two lists
    without duplicates.
    :param list_1: The first list.
    :type list_1: list.
    :param list_2: The second list.
    :type list_2: The second list.
    :return: The list of the numbers without duplicates.
    :rtype: list.
    """
    return list(set([x for x in list_1 for y in list_2 if x == y]))
