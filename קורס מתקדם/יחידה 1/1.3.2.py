def is_prime(number):
    """
    This function check if number is prime number.
    :param number: The number to check.
    :type number: int.
    :return: True or false if the number is a prime number.
    :rtype: bool.
    """
    return all([number % num for num in range(2, number)])
