def checkFour(number):
    """
    This function check if number divider with 4.
    :param number: The number to check.
    :type number: int.
    :return: True or false if divide.
    :rtype: bool.
    """
    return number % 4 == 0

def four_dividers(number):
    """
    This function get number and return all of the numbers in list between 1 and the number
    that divide with 4.
    :param number: The max number.
    :return: The list with the numbers that dividing with 4.
    :rtype: list.
    """
    return list(filter(checkFour, range(1, number)))
