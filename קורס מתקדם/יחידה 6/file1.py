class GreetingCard:
    """
    This class is a greeting card.
    """
    def __init__(self, recipient="Dana Ev", sender="Eyal Ch"):
        """
        This function init the greeting card with sender and recipient.
        :param recipient: The recipient name.
        :type recipient: str.
        :param sender: The sender name.
        :type sender: str.
        """
        self._recipient = recipient
        self._sender = sender

    def greeting_msg(self):
        """
        This function print the details of the greeting card, the name of sender and the recipient.
        :return: None.
        """
        print(f"sender: {self._sender}, recipient: {self._recipient}")
