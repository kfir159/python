import pyttsx3

def read_aloud():
    """
    This function using package to voice the sentence.
    :param sentence: The sentence to voice.
    :type sentence: str.
    :return: None.
    """
    sentence = "first time i'm using a package in next.py course"
    engine = pyttsx3.init()
    engine.say(sentence)
    engine.runAndWait()
    read_aloud(sentence)

def main():
    read_aloud()

if __name__ == '__main__':
    main()
