from file1 import GreetingCard

class BirthdayCard(GreetingCard):
    """
    This class represents a birthday card.
    """
    def __init__(self, sender_age=0):
        """
        This class init the birthday card with sender, recipient and age of the sender.
        :param sender_age: The age of the sender.
        :type sender_age: int.
        """
        super().__init__()
        self._sender_age = sender_age

    def greeting_msg(self):
        """
        This function print the details of the birthday card.
        :return: None.
        """
        super().greeting_msg()
        print(f"sender's age: {self._sender_age}")
