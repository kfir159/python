import tkinter as tk
from PIL import ImageTk, Image

def show_answer():
    """
    This function show the image after click on the button.
    :return: None.
    """
    answer_img = Image.open("ans.png")
    answer_img = answer_img.resize((200, 200))
    answer_img_tk = ImageTk.PhotoImage(answer_img)
    answer_label.configure(image=answer_img_tk)
    answer_label.image = answer_img_tk

window = tk.Tk()
window.title("Question and Answer")

question_label = tk.Label(window, text="Do you prefer Windows or Linux operating system?")
question_label.pack()

button = tk.Button(window, text="Show Answer", command=show_answer)
button.pack()

answer_label = tk.Label(window)
answer_label.pack()

window.mainloop()
