def is_prime(n):
    """
    This function check if number is prime number.
    :param n: The number to check.
    :type n: int.
    :return: True of false if the number is prime number.
    :rtype: bool.
    """
    # Corner case
    if n <= 1:
        return False
    # Check from 2 to n-1
    for i in range(2, n):
        if n % i == 0:
            return False
    return True

def first_prime_over(n):
    """
    This function find the first prime number after the number n.
    :param n: The number to find a prime number after him.
    :type n: int.
    :return: The first prime number after the number n.
    :rtype: int.
    """
    gen = (num for num in range(n, 10 * n) if is_prime(num))
    return next(gen)

print(first_prime_over(100000))
