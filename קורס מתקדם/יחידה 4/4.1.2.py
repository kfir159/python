def translate(sentence):
    """
    This function translate a sentence from spanish with generators and dict of words.
    :param sentence: The sentence to translate.
    :type sentence: str.
    :return: The sentence after the translation.
    :rtype: str.
    """
    words = {'esta': 'is', 'la': 'the', 'en': 'in', 'gato': 'cat', 'casa': 'house', 'el': 'the'}
    gen = (words[k] for k in sentence.split(' '))
    return ' '.join(gen)

print(translate("el gato esta en la casa"))
