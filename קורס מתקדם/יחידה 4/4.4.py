def gen_hours():
    """
    This function give all the hours.
    :return: The hours between 0 and 23.
    :rtype: generator.
    """
    hours = 0
    while hours < 24:
        yield hours
        hours += 1

def gen_minutes():
    """
    This function give all the minutes.
    :return: The minutes between 0 and 59.
    :rtype: generator.
    """
    minutes = 0
    while minutes < 60:
        yield minutes
        minutes += 1

def gen_secs():
    """
    This function give all the seconds.
    :return: The seconds between 0 and 59.
    :rtype: generator.
    """
    seconds = 0
    while seconds < 60:
        yield seconds
        seconds += 1

def gen_years(start=2023):
    """
    This function give all the years from the current year.
    :param start: The current year.
    :type start: int.
    :return: The year from the current year.
    :rtype: generator.
    """
    while True:
        yield start
        start += 1

def gen_months():
    """
    This function give all the months.
    :return: The months between 1 and 12.
    :rtype: generator.
    """
    month = 1
    while month < 12:
        yield month
        month += 1

def gen_days(month, leap_year=True):
    """
    This function give al the days in specific month and take care if it leap year.
    :param month: The current month.
    :type month: int.
    :param leap_year: If the current year is leap.
    :type leap_year: bool.
    :return: The days in the month.
    :rtype: generator.
    """
    day = 1
    if month == 1 or month == 3 or month == 5 or month == 7 or month == 8 or month == 10 or month == 12:
        while day <= 31:
            yield day
            day += 1
    elif month == 4 or month == 6 or month == 9 or month == 11:
        while day <= 30:
            yield day
            day += 1
    else:
        while day <= 28:
            yield day
            day += 1
        if leap_year:
            yield day

def gen_time():
    """
    This function give all of the times in day.
    :return: The all times in day.
    :rtype: generator.
    """
    hours = gen_hours()
    for hour in hours:
        minutes = gen_minutes()
        for minute in minutes:
            seconds = gen_secs()
            for second in seconds:
                yield "%02d:%02d:%02d" % (hour, minute, second)

def gen_date():
    """
    This function give all of the dates from the current year.
    :return: The all dates from the current year.
    :rtype: generator.
    """
    years = gen_years()
    for year in years:
        months = gen_months()
        for month in months:
            days = gen_days(month, (year % 4 == 0 and year % 100 != 0) or (year % 400 == 0))
            for day in days:
                yield "%02d/%02d/%04d" % (day, month, year)

def main():
    count = 0
    for gd in gen_date():
        the_date = gd
        for gt in gen_time():
            if(count == 1000000):
                print(the_date, gt)
                count = 0
            count += 1

if __name__ == '__main__':
    main()
