def parse_ranges(ranges_string):
    """
    This function get a string with ranges and print all of the numbers in those ranges.
    :param ranges_string: The string with the ranges.
    :type ranges_string: str.
    :return: Generator with all of the numbers in the ranges.
    :rtype: generator.
    """
    ranges = (ran.split("-") for ran in ranges_string.split(","))
    numbers = (num for start, end in ranges for num in range(int(start), int(end) + 1))
    return numbers

print(list(parse_ranges("1-2,4-4,8-10")))
print(list(parse_ranges("0-0,4-8,20-21,43-45")))
