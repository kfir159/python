def get_fibo():
    """
    This function return generator with the next value in fibonacci.
    :return: The next var in fibonacci.
    :rtype: generator.
    """
    a = 0
    b = 1
    while True:
        yield a
        c = a
        a = a + b
        b = c

fibo_gen = get_fibo()
print(next(fibo_gen))
print(next(fibo_gen))
print(next(fibo_gen))
print(next(fibo_gen))
