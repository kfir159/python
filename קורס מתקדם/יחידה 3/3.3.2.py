class UnderAgeException(Exception):
    """
    This class is exception for under 18 age.
    """

    def __init__(self, age):
        """
        This function init the exception with the age.
        :param age: The age that under 18.
        :type age: int.
        """
        self._age = age

    def __str__(self):
        """
        This function return the message error if the age under 18.
        :return: The message to the user.
        :rtype: str.
        """
        return "He under 18! His age is " + str(self._age) + " years old, and you will can invite him more " + str(18 - self._age) + " years"

def send_invitation(name, age):
    """
    This function send invitation if the age legal.
    :param name: The name of the invited.
    :type name: str.
    :param age: The age of the invited.
    :type age: int.
    :return: None.
    """
    try:
        if int(age) < 18:
            raise UnderAgeException(int(age))
        else:
            print("You should send an invite to " + name)
    except UnderAgeException as e:
        print(e)
    except ValueError as e:
        print("You have put a number.")

def main():
    name = input("Enter name: ")
    age = input("Enter age: ")
    send_invitation(name, age)

if __name__ == '__main__':
    main()
