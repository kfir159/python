def StopIteration():
    """
    This function implements the StopIteration exception.
    :return: None.
    """
    numbers = [1, 2, 3, 4, 5]
    iterator = iter(numbers)
    while True:
        number = next(iterator)

def ZeroDivisionError():
    """
    This function implements the ZeroDivisionError exception.
    :return: None.
    """
    print(4 / 0)

def AssertionError():
    """
    This function implements the AssertionError exception.
    :return: None.
    """
    x = 1
    y = 0
    assert y != 0, "Invalid Operation" # denominator can't be 0
    print(x / y)

def ImportError():
    """
    This function implements the ImportError exception.
    :return: None.
    """
    import hermelin

def KeyError():
    """
    This function implements the KeyError exception.
    :return: None.
    """
    dict = {1 : "f"}
    print(dict[2])

def SyntaxError()
    """
    This function implements the SyntaxError exception.
    :return: None.
    """
    print("hello?")

def IndentationError():
    """
    This function implements the IndentationError exception.
    :return: None.
    """
print("hello?")

def TypeError():
    """
    This function implements the TypeError exception.
    :return: None.
    """
    print(3 / "4")

def main():
    #StopIteration()
    #ZeroDivisionError()
    #AssertionError()
    #ImportError()
    #KeyError()
    #SyntaxError()
    #IndentationError()
    #TypeError()

if __name__ == '__main__':
    main()
