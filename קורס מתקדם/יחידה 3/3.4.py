import string

class UsernameContainsIllegalCharacter(Exception):
    """
    This class represents an exception when the user wrote illegal char in the username.
    """

    def __init__(self, char, index):
        """
        This function init the exception with the illegal char and his index in the username.
        :param char: The char that illegal.
        :param index: The index of the char.
        """
        self._char = char
        self._index = index

    def __str__(self):
        """
        This function return the details about the exception.
        :return: The details.
        :rtype: str.
        """
        return "You have the wrong char '%s' in your username at index %s." % (self._char, str(self._index))

class UsernameTooShort(Exception):
    """
    This class represents an exception that the username is too short.
    """

    def __str__(self):
        """
        This function return the details about the exception.
        :return: The details.
        :rtype: str.
        """
        return "Your username shorter then 3 chars."

class UsernameTooLong(Exception):
    """
    This class represents an exception that the username is too long.
    """

    def __str__(self):
        """
        This function return the details about the exception.
        :return: The details.
        :rtype: str.
        """
        return "Your username longer then 16 chars."

class PasswordMissingCharacter(Exception):
    """
    This class represents an exception that the password missing character that must write.
    """

    def __str__(self):
        """
        This function return the details about the exception.
        :return: The details.
        :rtype: str.
        """
        return "You don't have one or more from the must chars."

class PasswordMissingCharacterUppercase(PasswordMissingCharacter):
    """
    This class extends from PasswordMissingCharacter and represents an exception that the user
    miss specific uppercase char in his password.
    """

    def __str__(self):
        """
        This function return the details about the exception.
        :return: The details.
        :rtype: str.
        """
        return super().__str__() + " (Uppercase)"

class PasswordMissingCharacterLowercase(PasswordMissingCharacter):
    """
    This class extends from PasswordMissingCharacter and represents an exception that the user
    miss specific lowercase char in his password.
    """

    def __str__(self):
        """
        This function return the details about the exception.
        :return: The details.
        :rtype: str.
        """
        return super().__str__() + " (Lowercase)"

class PasswordMissingCharacterDigit(PasswordMissingCharacter):
    """
    This class extends from PasswordMissingCharacter and represents an exception that the user
    miss specific digit char in his password.
    """

    def __str__(self):
        """
        This function return the details about the exception.
        :return: The details.
        :rtype: str.
        """
        return super().__str__() + " (Digit)"

class PasswordMissingCharacterSpecial(PasswordMissingCharacter):
    """
    This class extends from PasswordMissingCharacter and represents an exception that the user
    miss specific special char in his password.
    """

    def __str__(self):
        """
        This function return the details about the exception.
        :return: The details.
        :rtype: str.
        """
        return super().__str__() + " (Special)"

class PasswordTooShort(Exception):
    """
    This class represents an exception that the password is too short.
    """

    def __str__(self):
        """
        This function return the details about the exception.
        :return: The details.
        :rtype: str.
        """
        return "Your password shorter then 8 chars."

class PasswordTooLong(Exception):
    """
    This class represents an exception that the password is too long.
    """

    def __str__(self):
        """
        This function return the details about the exception.
        :return: The details.
        :rtype: str.
        """
        return "Your password longer then 40 chars."

def check_input(username, password):
    """
    This function check if the username and the password that the user wrote are valid.
    :param username: The username of the user.
    :type username: str.
    :param password: The password of the user.
    :type password: str.
    :return: True of false if the username and the password are valid.
    :rtype: bool.
    """
    try:
        if len(username) < 3:
            raise UsernameTooShort
        elif len(username) > 16:
            raise  UsernameTooLong
        elif len(username) >= 3 and len(username) <= 16:
            for char in username:
                if not char.isalpha() and not char.isdigit() and char != '_':
                    raise UsernameContainsIllegalCharacter(char, username.find(char))
        if len(password) < 8:
            raise PasswordTooShort
        elif len(password) > 40:
            raise PasswordTooLong
        else:
            has_uppercase = False
            has_lowercase = False
            has_digit = False
            has_special = False

            for char in password:
                if char.isupper():
                    has_uppercase = True
                elif char.islower():
                    has_lowercase = True
                elif char.isdigit():
                    has_digit = True
                elif char in string.punctuation:
                    has_special = True

            if not has_uppercase:
                raise PasswordMissingCharacterUppercase
            elif not has_lowercase:
                raise PasswordMissingCharacterLowercase
            elif not has_digit:
                raise PasswordMissingCharacterDigit
            elif not has_special:
                raise PasswordMissingCharacterSpecial
            else:
                print("OK")
                return True
    except Exception as e:
        print(e)
        return False

def main():
    valid = False
    while not valid:
        username = input("Enter username: ")
        password = input("Enter password: ")
        valid = check_input(username, password)

if __name__ == '__main__':
    main()
