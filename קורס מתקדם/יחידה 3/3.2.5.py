def read_file(file_name):
    """
    This function check if file is exist and if yes print his text and else print that the file doesn't exist.
    :param file_name: The name of the file.
    :type file_name: str.
    :return: Text according the file.
    :rtype: str.
    """
    try:
        file = open(file_name)
        try:
            file_text = file.read()
            return "__CONTENT_START__\n" + file_text + "\n__CONTENT_END__"
        except IOError:
            return "The file is defective"
        finally:
            file.close()
    except IOError:
        return """__CONTENT_START__
__NO_SUCH_FILE__
__CONTENT_END__"""
