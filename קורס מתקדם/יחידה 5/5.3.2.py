class MusicNotes:
    """
    This class represents a music notes and implements an iterator.
    """
    def __init__(self):
        """
        This function init the class with the notes and their octaves.
        """
        self.notes = [55, 61.74, 65.41, 73.42, 82.41, 87.31, 98]
        self.num_octave = 5
        self.curr_octave = 0

    def __iter__(self):
        """
        This function override the function iter for the iterator.
        :return: The iterator.
        :rtype: iterator.
        """
        return self

    def __next__(self):
        """
        This function override the function next for the iterator and return the next value from the list of the octaves.
        :return: The curr octaves.
        :rtype: int.
        """
        if self.curr_octave >= self.num_octave * len(self.notes):
            raise StopIteration
        result = self.notes[self.curr_octave // self.num_octave] * (2 ** (self.curr_octave % self.num_octave))
        self.curr_octave += 1
        return result

def main():
    notes_iter = iter(MusicNotes())

    for freq in notes_iter:
        print(freq)

if __name__ == '__main__':
    main()
