def check_id_valid(id_number):
    """
    This function check if id is valid.
    :param id_number: The id to check.
    :type id_number: int.
    :return: True or false if the id is valid.
    :rtype: bool.
    """
    digits = [int(digit) for digit in str(id_number)]
    result = [digit * 1 if index % 2 == 0 else digit * 2 for index, digit in enumerate(digits)]
    final = []
    for num in result:
        if num > 9:
            final += [sum(int(digit) for digit in str(num))]
        else:
            final += [num]
    return sum(final) % 10 == 0

class IDIterator:
    """
    This class is a iterator that give valid id.
    """

    def __init__(self, id):
        """
        This function init the class iterator with start id.
        :param id: The id the give valid id from him.
        :type id: int.
        """
        self._id = id

    def __iter__(self):
        """
        This function override the function iter to be an iterator class.
        :return: The iterator.
        :rtype: iterator.
        """
        return self

    def __next__(self):
        """
        This function override the function next that give the next valid id from the iterator.
        :raise: StopIteration: If there is no more values from the iterator.
        :return: The valid id.
        :rtype: int.
        """
        if self._id > 999999999:
            raise StopIteration

        valid = False
        while not valid:
            self._id += 1
            valid = check_id_valid(self._id)
        return self._id

def id_generator(id_number):
    """
    This function is a generator function that give valid id.
    :param id_number: The start id to give valid id from him.
    :return: Valid id.
    :rtype: int.
    """
    while id_number < 999999999:
        id_number += 1
        if check_id_valid(id_number):
            yield id_number

def main():
    id = int(input("Enter ID: "))
    type = input("Generator or Iterator? (gen/it)? ")
    if type == "it":
        it = IDIterator(id)
        for i in range(1, 11):
            print(next(it))
    elif type == "gen":
        gen = id_generator(id)
        for i in range(1, 11):
            print(next(gen))
    else:
        print("invalid option.")

if __name__ == '__main__':
    main()
