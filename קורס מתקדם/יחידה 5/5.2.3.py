import itertools

def calculate_options():
    """
    This function find the options to get sum of 100 dollars from specific bills.
    :return: All the combines.
    :rtype: set.
    """
    available_bills = {20: 3, 10: 5, 5: 2, 1: 5}
    options = set()

    for i in range(1, 101):
        combinations = itertools.combinations_with_replacement(available_bills.keys(), i)
        for comb in combinations:
            if sum(comb) == 100:
                bill_counts = {bill: comb.count(bill) for bill in comb}
                if all(count <= available_bills[bill] for bill, count in bill_counts.items()):
                    sorted_comb = tuple(sorted(comb, reverse=True))
                    options.add(sorted_comb)
    return options

def main():
    options = calculate_options()
    for option in options:
        print(option)

    print("Total options:", len(options))

if __name__ == '__main__':
    main()
