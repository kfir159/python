import winsound

def little_Jonathan():
    """
    This function play the song little_Jonathan with beeps.
    :return: None.
    """
    freqs = {"la": 220,
             "si": 247,
             "do": 261,
             "re": 293,
             "mi": 329,
             "fa": 349,
             "sol": 392,
             }

    notes = "sol,250-mi,250-mi,500-fa,250-re,250-re,500-do,250-re,250-mi,250-fa,250-sol,250-sol,250-sol,500"
    notes_list = notes.split("-")
    notes_iter = iter(notes_list)
    for note in notes_iter:
        curr_note = note.split(",")
        winsound.Beep(freqs[curr_note[0]], int(curr_note[1]))

def main():
    little_Jonathan()

if __name__ == '__main__':
    main()
